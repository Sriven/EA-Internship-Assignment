// All headers files included below

#include <SFML/Graphics.hpp>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <unistd.h>
#include <string>

using namespace std;

const int columns = 8;
const int rows = 8;

// The class gamelogic contains all the variables for the proper functioning of the game Gems. 

class gamelogic
{
    int columns;
    int rows;
    bool isEnterPressed;
    int score, scoremult, moves;

    sf::RenderWindow window;
    sf::Vector2f cellSize;
    sf::Vector2f cellSizebox;
    sf::Font font;
    sf::Text scoretext;
    sf::Text movestext;


public:

    sf::Texture texture[5];

    gamelogic()             // a contructor which initializes all the variables to their correct values.
    {
        columns = 8;
        rows = 8;
        isEnterPressed = false;
        score = 0;
        scoremult = 1;
        moves = 30;
        window.create(sf::VideoMode(600, 600), "Gems");
        cellSize.x = 60.0f;
        cellSize.y = 60.0f;
        cellSizebox.x = 57.0f;
        cellSizebox.y = 57.0f;

        texture[0].loadFromFile("Resources/Diamond.png");
        texture[1].loadFromFile("Resources/Triangle.png");
        texture[2].loadFromFile("Resources/Square.png");
        texture[3].loadFromFile("Resources/Dode.png");
        texture[4].loadFromFile("Resources/Hexagon.png");
        

        font.loadFromFile("Resources/RobotoMono-Regular.ttf");

        scoretext.setFont(font);
        scoretext.setColor(sf::Color::White);
        scoretext.setCharacterSize(25);
        scoretext.setPosition(420.0f, 0.0f);

        movestext.setFont(font);
        movestext.setColor(sf::Color::White);
        movestext.setCharacterSize(25);
        movestext.setPosition(0.0f, 0.0f);

    }

// return funcions which the values of initialzed variables to where its needed.

    int getColumns()
    {
        return columns;
    }

    int getRows()
    {
        return rows;
    }

    bool getEnterState()
    {
        return isEnterPressed;
    }

    void setEnterState(bool t)
    {
        isEnterPressed = t;
    }

    int CalcScore()
    {
        score += 10 * scoremult;
    }

    void setScoreMult(int mult)
    {
        scoremult = mult;
    }

    int setMoves(int a)
    {
        moves += a;
    }

    sf::RenderWindow* getWindow()
    {
        return &window;
    }

    sf::Vector2f getCellSize()
    {
        return cellSize;
    }

    sf::Vector2f getCellSizeBox()
    {
        return cellSizebox;
    }

    sf::Font getFont()
    {
        return font;
    }

    // three function which perform the main logic.

    void checkformoves(); //  The checker function checks if all the moves hasbeen exhausted.

    void disp(); // The disp() function displays the grid and gems on the screen.

    void GameRun(); //  The GameRun() function handles all the events that take place in the window and takes all the keyboard inputs from the user.
    
};

gamelogic game;

// The class Grid Encapsulates the code needed to make the grid and hold gems.

class gr
{
    int typeofimg;
    sf::RectangleShape box;

public:

    gr()            // a contructor to initialize the objects of the class.
    {
        box.setSize(game.getCellSize());
        box.setOutlineColor(sf::Color::Cyan);
        box.setOutlineThickness(2.5f);
    }

    void settexture(int a) // The settexture() function sets the texture of the rectangular grid boxes as gems such that each box holds only one gem.
    {
        box.setTexture(&game.texture[a]);
    }
    void setposition(float a, float b)
    {
        box.setPosition(a, b);
    }
    void drawbox()
    {
        game.getWindow()->draw(box);
    }
    int getint() //  The getint() function returns what gem the current box is holding.
    {
        return typeofimg;
    }
    void setint(int x) // The setint() function sets the box to hold one type of gem.
    {
        typeofimg = x;
    }
};

gr grid[columns][rows]; 

// The class rect encapsulates the code for the selection box and swapping the gems.

class rect
{
    int x, y;
    sf::RectangleShape select;
    bool matchNotOccured;

public:

    rect()              // It has a contructor to intialize the box and set its properties.
    {
        x = 1;
        y = 1;
        matchNotOccured = true;
        select.setSize(game.getCellSizeBox());
        select.setOutlineColor(sf::Color(255, 140, 0, 255));
        select.setFillColor(sf::Color::Transparent);
        select.setOutlineThickness(5.0f);     
        select.setPosition(x * game.getCellSize().x, y * game.getCellSize().y);
    }

    //  It has a number of functions which provide the necessary tools to swap and move the selection box.

    void moveBox(int a, int b)  // The moveBox() function moves the selection box from one tile to the adjacent tile.

       { 
        if ((x == 1 && a == -1) || (x == 8 && a == 1) || (y == 1 && b == -1) || (y == 8 && b == 1))
            return;

        else
        {
            x += a;
            y += b;
        }

        select.setPosition(x * game.getCellSize().x, y * game.getCellSize().y);
    }
    void drawbox() // DrawBox() function draws the selection box to the screen.
    {
        game.getWindow()->draw(select);
    }
    void pushdown(int i, int j, int num) // The pushdown() function is a utility function that pushes new elements into the grid after a match has been made.
    {
        while (i > (num - 1))
        {
            grid[i][j].settexture(grid[i - num][j].getint());
            grid[i][j].setint(grid[i - num][j].getint());
            i--;
        }
        i = 0;

        while (i <= (num - 1))
        {
            int random = rand() % 5;
            grid[i][j].settexture(random);
            grid[i][j].setint(random);
            i++;
        }

        game.disp();
    }

    void match3() // match3() function checks if the match has occerred either horizontally or vertically and then calls the pushdown function.
    {

        for (int i = 0; i < game.getColumns(); i++)
        {
            for (int j = 0; j < game.getRows(); j++)
            {      
                if (i > 1)
                {
                    if (grid[i][j].getint() == grid[i - 1][j].getint()) 
                    {
                        if (grid[i][j].getint() == grid[i - 2][j].getint())
                        {
                            matchNotOccured = false;
                            game.CalcScore();
                            game.setScoreMult(2);
                            pushdown(i, j, 3);    
                            sleep(1);
                            match3();             
                        }
                    }
                }
                if (j > 1)
                {
                    if (grid[i][j].getint() == grid[i][j - 1].getint())
                    {
                        if (grid[i][j].getint() == grid[i][j - 2].getint())
                        {
                            matchNotOccured = false;
                            game.CalcScore();
                            game.setScoreMult(2);
                            pushdown(i, j, 1);
                            pushdown(i, j - 1, 1);
                            pushdown(i, j - 2, 1);
                            sleep(1);
                            match3();
                        }
                    }
                }
            }
        }

    }
    void switchtex(int a, int b) // The switchtex() function will be called to swap two gems positions.

    {

        if ((x == 1 && a == -1) || (x == 8 && a == 1) || (y == 1 && b == -1) || (y == 8 && b == 1))
            return;

        int tex1 = grid[y - 1][x - 1].getint();
        int tex2 = grid[y + b - 1][x + a - 1].getint();

        grid[y - 1][x - 1].settexture(tex2);
        grid[y + b - 1][x + a - 1].settexture(tex1);

        grid[y - 1][x - 1].setint(tex2);
        grid[y + b - 1][x + a - 1].setint(tex1);

        game.setScoreMult(1);
        game.setMoves(-1);
        matchNotOccured = true;
        match3();

        if (matchNotOccured) // If a match did not occur after swapping, it'll throw a message and reset the move
        {
            sf::Text err;
            sf::Font font2;
            font2.loadFromFile("Resources/RobotoMono-Bold.ttf");
            err.setFont(font2);
            err.setColor(sf::Color::White);
            err.setCharacterSize(30);
            err.setPosition(10.0f, 10.0f);
            err.setString("A Match has not occurred.\nResetting the move.");
            game.getWindow()->clear();
            game.getWindow()->draw(err);
            game.getWindow()->display();
            sleep(2);

            tex1 = grid[y - 1][x - 1].getint();
            tex2 = grid[y + b - 1][x + a - 1].getint();

            grid[y - 1][x - 1].settexture(tex2);
            grid[y + b - 1][x + a - 1].settexture(tex1);

            grid[y - 1][x - 1].setint(tex2);
            grid[y + b - 1][x + a - 1].setint(tex1);

            game.setMoves(1);
        }

        game.setEnterState(false);
    }
};

rect box;


void setGrid() // The setGrid() function is called once to create the grid in the start.
{
    bool flag = true;

    for (int i = 0; i < game.getColumns(); i++)
    {
        for (int j = 0; j < game.getRows(); j++)
        {
            int random = rand() % 5;
            flag = true;

            while (flag)
            {
                flag = false;
                
                if (i > 1)
                {
                    if (random == grid[i - 1][j].getint())
                    {
                        if (random == grid[i - 2][j].getint())
                        {
                            flag = true;
                            random = rand() % 5;                        
                        }
                    }
                }
                if (j > 1)
                {
                    if (random == grid[i][j - 1].getint())
                    {
                        if (random == grid[i][j - 2].getint())
                        {
                            flag = true;
                            random = rand() % 5;                        
                        }
                    }
                }
            }

            grid[i][j].settexture(random);
            grid[i][j].setint(random);

            grid[i][j].setposition((j + 1)*game.getCellSize().x, (i + 1)*game.getCellSize().y);
        }
    }
}

void gamelogic::disp()
{

    string scorestring = "Score = ";
    scorestring.append(to_string(score));
    scoretext.setString(scorestring);

    string movestring = "Moves Left = ";
    movestring.append(to_string(moves));
    movestext.setString(movestring);

    window.clear();

    for (int i = 0; i < columns; i++)
    {
        for (int j = 0; j < rows; j++)
        {
            grid[i][j].drawbox();
        }
    }

    window.draw(scoretext);
    window.draw(movestext);
    box.drawbox();

    window.display();
}

void gamelogic::checkformoves()
{
    if (moves <= 0)
    {
        window.clear();
        sf::Text gameover;
        gameover.setFont(font);
        gameover.setColor(sf::Color::White);
        gameover.setCharacterSize(20);

        string gameovertext = "GAME OVER! \nFinal Score = ";
        gameovertext.append(to_string(score));
        gameover.setString(gameovertext);
        window.draw(gameover);
        window.display();

        sleep(2);
        exit(0);
    }
}

void gamelogic::GameRun()
{
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
                window.close();

            if (event.type == sf::Event::KeyPressed)
            {
                if (event.key.code == sf::Keyboard::Escape)
                {
                    isEnterPressed = false;
                }

                if (isEnterPressed == true)
                {
                    switch (event.key.code)
                    {
                        case sf::Keyboard::Left:
                            box.switchtex(-1, 0);
                            break;

                        case sf::Keyboard::Right:
                            box.switchtex(1, 0);
                            break;

                        case sf::Keyboard::Up:
                            box.switchtex(0, -1);
                            break;

                        case sf::Keyboard::Down:
                            box.switchtex(0, 1);
                            break;
                    }

                }
                else
                    switch (event.key.code)
                    {
                        case sf::Keyboard::Left:
                            box.moveBox(-1, 0);
                            break;

                        case sf::Keyboard::Right:
                            box.moveBox(1, 0);
                            break;

                        case sf::Keyboard::Up:
                            box.moveBox(0, -1);
                            break;

                        case sf::Keyboard::Down:
                            box.moveBox(0, 1);
                            break;

                        case sf::Keyboard::Return:
                            isEnterPressed = true;

                        default:
                            break;
                    }
            }
        }

        disp();
        checkformoves();
    }

}

int main()
{
    srand ((int) time(0));

    setGrid();

    game.GameRun();

    return 0;
}
 